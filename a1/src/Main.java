import java.sql.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
//        System.out.println("Hello world!");
        HashMap<String,Integer> games = new HashMap<>();
        games.put("Mario Odyssey", 50);
        games.put("Super Smash Bros. Ultimate", 20);
        games.put("Luigi's Mansion 3", 15);
        games.put("Pokemon Sword", 30);
        games.put("Pokemon Shield", 100);

        games.forEach((key,value) -> {
            System.out.println(key + " has " + value + " stocks left.");
        });

        ArrayList<String> topGames = new ArrayList<>();
        games.forEach((key,value) -> {
            if(value <= 30){
                topGames.add(key);
                System.out.println(key + " has been added to top games list!");
            }
        });

        System.out.println(topGames);

        boolean addItem = true;
        Scanner userInput = new Scanner(System.in);
        while(addItem){
            System.out.println("Would you like to add an item? Yes or No.");
            String input = userInput.nextLine();
            String yes = "Yes";
            String no = "No";
//            System.out.println(input);

            if(yes.equalsIgnoreCase(input)){
                System.out.println("Add the item name");
                String itemName = userInput.nextLine();

                System.out.println("Add the item stock");
                int stock = userInput.nextInt();

                games.put(itemName,stock);
                addItem = false;
            } else if (no.equalsIgnoreCase(input)){
                addItem = false;
                System.out.println("Thank you.");
            }
        }
        System.out.println(games);
    }
}